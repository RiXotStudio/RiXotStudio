#!/usr/bin/env nix-shell
# NixOS shell configuration to bootstrap the required dependencies to work with the source code

{
	sources ? import ./25-env/nix/niv/sources.nix,
	nixpkgs ? import sources.nixpkgs {}
}:
with nixpkgs;
	stdenv.mkDerivation {
		name = "RiXotStudio";
		buildInputs = [
			niv # Used to update the pinning of nix environment for packages
			cargo-make # USed to interpret `Makefile.toml` to interact with the repository
		];

		shellHook = ''
			# Output status update
			printf "\\n%s\\n" "Standard RiXotStudio development environment has been set up"
		'';
	}
