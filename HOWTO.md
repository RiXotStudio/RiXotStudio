### How to work with the source code

This is a [**monorepository** ](https://en.wikipedia.org/wiki/Monorepo) composed of multiple git repositories that are defined as git submodules to increase the likelyhood of clean commit history and controlled project management for mission critical usage.  

The git submodules are designed to be independent from one another and each provide a standardized:  

1. development environment (e.g. dependencies needed to work with the project)  
2. relevant software configuration (e.g. text editor configurationTo utilize these  

Refer to your system administrator or developer for instructions on how to set up reproducible environment in this project or create an issue tracking in our issue tracker in case we didn't provide the required standardization for solution that is capable of [**reproducible builds**](https://en.wikipedia.org/wiki/Reproducible_builds).  